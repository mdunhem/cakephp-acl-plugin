<?php

?>
<?php if ($node['AclAro']['lft']) { ?>
    <option value="<?php echo $node['AclAro']['parent_id']; ?>">..</option>
<?php } ?>
<option id="aro_editor_defaultParentId" value="<?php echo $node['AclAro']['id']; ?>">.</option>
<?php foreach ($children as $c) { ?>
    <option value="<?php echo $c['AclAro']['id']; ?>">
        <?php echo $c['AclAro']['alias']; ?>
        <?php if ($c['AclAro']['children']) {
            echo " ({$c['AclAro']['children']})";
        } ?>
    </option>
<?php } ?>
