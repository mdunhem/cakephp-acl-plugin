<?php

?>
<?php if ($node['AclAco']['lft']) { ?>
    <option value="<?php echo $node['AclAco']['parent_id']; ?>">..</option>
<?php } ?>
<option id="aco_editor_defaultParentId" value="<?php echo $node['AclAco']['id']; ?>">.</option>
<?php foreach ($children as $c) { ?>
    <option value="<?php echo $c['AclAco']['id']; ?>">
        <?php echo $c['AclAco']['alias']; ?>
        <?php if ($c['AclAco']['children']) {
            echo " ({$c['AclAco']['children']})";
        } ?>
    </option>
<?php } ?>
