<?php

?>
<?php echo $this->element('acl_scripts') ?>
<h2>Access Control List Management</h2>
<ul class="acl">
    <li>
        <?php echo $this->Html->image('/acl/img/tango/32x32/apps/system-users.png'); ?>
        <?php echo $this->Html->link('Manage Aros', array('plugin' => 'acl', 'prefix' => 'admin', 'controller' => 'acl', 'action' => 'aros')); ?>
    </li>
    <li>
        <?php echo $this->Html->image('/acl/img/tango/32x32/apps/preferences-system-windows.png'); ?>
        <?php echo $this->Html->link('Manage Acos', array('plugin' => 'acl', 'prefix' => 'admin', 'controller' => 'acl', 'action' => 'acos')); ?>
    </li>
    <li>
        <?php echo $this->Html->image('/acl/img/tango/32x32/emblems/emblem-readonly.png'); ?>
        <?php echo $this->Html->link('Manage Permissions', array('plugin' => 'acl', 'prefix' => 'admin', 'controller' => 'acl', 'action' => 'permissions')); ?>
    </li>
</ul>
<br />

<h2>Quick Start</h2><br />

<div>
    <strong>ARO - Access Request Object</strong>
    <br />
    Things (most often users) that want to use stuff are called access request objects
</div>
<br />

<div>
    <strong>ACO - Access Control Object</strong>
    <br />
    Things in the system that are wanted (most often actions or data) are called access control objects
</div>

<h2>Known Bugs</h2>
<ul>
    <li>does not show inherited permissions</li>
    <li>does not show full path in finder</li>
    <li>does not have crud fields</li>
</ul>
